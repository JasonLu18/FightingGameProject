// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "FightingGameProjectCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

AFightingGameProjectCharacter::AFightingGameProjectCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Rotation of the character should not affect rotation of boom
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->SocketOffset = FVector(0.f, 0.f, 75.f);
	CameraBoom->SetRelativeRotation(FRotator(0.f, 180.f, 0.f));

	// Create a camera and attach to boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->bUsePawnControlRotation = false; // We don't want the controller rotating the camera

	// Configure character movement
	GetCharacterMovement()->GravityScale = 2.f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	GetCharacterMovement()->MaxFlySpeed = 600.f;
	GetCharacterMovement()->bOrientRotationToMovement = false; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // ...at this rotation rate

	otherPlayer = nullptr;
	isFlipped = false;
	transform = FTransform();
	scale = FVector(0.0f, 0.0f, 0.0f);

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character)
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}


void AFightingGameProjectCharacter::Crouch(float Value)
{

	IsCrouching = false;
	if (Value > 0.5) {
		IsCrouching = true;
		// UE_LOG(LogTemp, Warning, TEXT("Crouch"));
	}
}

void AFightingGameProjectCharacter::MoveRight(float Value)
{
	if (Value > 0.20f)
	{
		directionalInput = EDirectionalInput::VE_MovingRight;
	}

	else if (Value < -0.20f)
	{
		directionalInput = EDirectionalInput::VE_MovingLeft;
	}

	else
	{
		directionalInput = EDirectionalInput::VE_Default;
	}

	// add movement in that direction if not crouching
	if (IsCrouching == false) {
		AddMovementInput(FVector(0.f, -1.f, 0.f), Value);
	}
}

void AFightingGameProjectCharacter::MoveLeft(float Value)
{
	// add movement in that direction
	if (IsCrouching == false) {
		AddMovementInput(FVector(0.f, 1.f, 0.f), Value);
	}
}

void AFightingGameProjectCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// check if null 
	if (otherPlayer) 
	{
		if (auto characterMovement = GetCharacterMovement())
		{
			if (auto enemyMovement = otherPlayer->GetCharacterMovement())
			{
				if (enemyMovement->GetActorLocation().Y >= characterMovement->GetActorLocation().Y)
				{
					if (isFlipped)
					{
						if (auto mesh = GetCapsuleComponent()->GetChildComponent(1))
						{
							transform = mesh->GetRelativeTransform();
							scale = transform.GetScale3D();
							scale.Y = -1;
							transform.SetScale3D(scale);
							mesh->SetRelativeTransform(transform);
							UE_LOG(LogTemp, Warning, TEXT("True"));
						}
						isFlipped = false;
					}
				}
				else
				{
					if (!isFlipped)
					{
						if (auto mesh = GetCapsuleComponent()->GetChildComponent(1))
						{
								transform = mesh->GetRelativeTransform();
								scale = transform.GetScale3D();
								scale.Y= 1;
								transform.SetScale3D(scale);
								mesh->SetRelativeTransform(transform);
								UE_LOG(LogTemp, Warning, TEXT("False"));
						}
						isFlipped = true;
					}
				}
			}
		}
	}
}
