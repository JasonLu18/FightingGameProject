// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "BaseGameInstance.generated.h"

UENUM(BlueprintType)
enum class ECharacterClass : uint8
{
	VE_Default UMETA(DisplayName = "Ninja"),
	VE_Ninja UMETA(DisplayName = "Ninja"),
	VE_Mage UMETA(DisplayName = "Mage")
};

UCLASS()
class FIGHTINGGAMEPROJECT_API UBaseGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player References")
		ECharacterClass characterClass;
};
