// Fill out your copyright notice in the Description page of Project Settings.


#include "FemaleNinja.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include <string>
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

using namespace std;

AFemaleNinja::AFemaleNinja() {

	// PlayerStats
	PlayerHealth = 2.00f;
	UE_LOG(LogTemp, Warning, TEXT("Female Ninja constructed"));
}

void AFemaleNinja::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFightingGameProjectCharacter::MoveRight);
	PlayerInputComponent->BindAxis("MoveLeft", this, &AFightingGameProjectCharacter::MoveLeft);
	PlayerInputComponent->BindAxis("Crouch", this, &AFightingGameProjectCharacter::Crouch);

	PlayerInputComponent->BindAction("Attack1", IE_Pressed, this, &AFemaleNinja::StandingLP);
	PlayerInputComponent->BindAction("Attack2", IE_Pressed, this, &AFemaleNinja::StandingMP);
	PlayerInputComponent->BindAction("Attack3", IE_Pressed, this, &AFemaleNinja::StandingHP);
	PlayerInputComponent->BindAction("Attack4", IE_Pressed, this, &AFemaleNinja::StandingLK);
	PlayerInputComponent->BindAction("Attack5", IE_Pressed, this, &AFemaleNinja::StandingMK);
	PlayerInputComponent->BindAction("Attack6", IE_Pressed, this, &AFemaleNinja::StandingHK);

}

void AFemaleNinja::TakeDamage(float damageAmount)
{
	PlayerHealth -= damageAmount;
	if (PlayerHealth < 0.00f) {
		PlayerHealth = 0.00f;
	}
}

void AFemaleNinja::StandingLP()
{
	if (IsCrouching == false && GetCharacterMovement()->IsFalling() == false)
	{
		CurrentAttack = "StLP";
		UE_LOG(LogTemp, Warning, TEXT("St. LP"));
	}

	if (IsCrouching == true && GetCharacterMovement()->IsFalling() == false)
	{
		CurrentAttack = "CrLP";
		UE_LOG(LogTemp, Warning, TEXT("Cr. LP"));
	}

	if (GetCharacterMovement()->IsFalling() == true)
	{
		UE_LOG(LogTemp, Warning, TEXT("Jump. LP"));
	}
}

void AFemaleNinja::StandingMP()
{
	if (IsCrouching == false && GetCharacterMovement()->IsFalling() == false)
	{
		CurrentAttack = "StMP";
		UE_LOG(LogTemp, Warning, TEXT("St. MP"));
	}

	if (IsCrouching == true && GetCharacterMovement()->IsFalling() == false)
	{
		CurrentAttack = "CrMP";
		UE_LOG(LogTemp, Warning, TEXT("Cr. MP"));
	}

	if (GetCharacterMovement()->IsFalling() == true)
	{
		UE_LOG(LogTemp, Warning, TEXT("Jump. MP"));
	}
}

void AFemaleNinja::StandingHP()
{
	if (IsCrouching == false && GetCharacterMovement()->IsFalling() == false)
	{
		CurrentAttack = "StHP";
		UE_LOG(LogTemp, Warning, TEXT("St. HP"));
	}

	if (IsCrouching == true && GetCharacterMovement()->IsFalling() == false)
	{
		CurrentAttack = "CrHP";
		UE_LOG(LogTemp, Warning, TEXT("Cr. HP"));
	}

	if (GetCharacterMovement()->IsFalling() == true)
	{
		UE_LOG(LogTemp, Warning, TEXT("Jump. HP"));
	}
}

void AFemaleNinja::StandingLK()
{
	if (IsCrouching == false && GetCharacterMovement()->IsFalling() == false)
	{
		CurrentAttack = "StLK";
		UE_LOG(LogTemp, Warning, TEXT("St. LK"));
	}

	if (IsCrouching == true && GetCharacterMovement()->IsFalling() == false)
	{
		CurrentAttack = "CrLK";
		UE_LOG(LogTemp, Warning, TEXT("Cr. LK"));
	}

	if (GetCharacterMovement()->IsFalling() == true)
	{
		UE_LOG(LogTemp, Warning, TEXT("Jump. LK"));
	}
}

void AFemaleNinja::StandingMK()
{

	if (IsCrouching == false && GetCharacterMovement()->IsFalling() == false)
	{
		CurrentAttack = "StMK";
		UE_LOG(LogTemp, Warning, TEXT("St. MK"));
	}

	if (IsCrouching == true && GetCharacterMovement()->IsFalling() == false)
	{
		CurrentAttack = "CrMK";
		UE_LOG(LogTemp, Warning, TEXT("Cr. MK"));
	}

	if (GetCharacterMovement()->IsFalling() == true)
	{
		UE_LOG(LogTemp, Warning, TEXT("Jump. MK"));
	}
}

void AFemaleNinja::StandingHK()
{
	if (IsCrouching == false && GetCharacterMovement()->IsFalling() == false)
	{
		CurrentAttack = "StHK";
		UE_LOG(LogTemp, Warning, TEXT("St. HK"));
	}

	if (IsCrouching == true && GetCharacterMovement()->IsFalling() == false)
	{
		CurrentAttack = "CrHK";
		UE_LOG(LogTemp, Warning, TEXT("Cr. HK"));
	}

	if (GetCharacterMovement()->IsFalling() == true)
	{
		UE_LOG(LogTemp, Warning, TEXT("Jump. HK"));
	}
}

