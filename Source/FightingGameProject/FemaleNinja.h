// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <string>
#include "FightingGameProjectCharacter.h"
#include "FemaleNinja.generated.h"

using namespace std;
UCLASS()
class FIGHTINGGAMEPROJECT_API AFemaleNinja : public AFightingGameProjectCharacter
{
	GENERATED_BODY()

public:
	AFemaleNinja();

protected:
	void StandingLP();
	void StandingMP();
	void StandingHP();
	void StandingLK();
	void StandingMK();
	void StandingHK();

	// APawn interface
	void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

	void TakeDamage(float damageAmount);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ForwardMovement")
		bool WalkingForward = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BackwardMovement")
		bool WalkingBackward = false;

	UPROPERTY(BlueprintReadWrite, Category = "Animation")
		FString CurrentAttack = "null";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		bool IsStanding = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		bool IsJumping = true;


};
