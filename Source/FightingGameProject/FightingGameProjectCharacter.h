// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include <string>
#include "GameFramework/Character.h"
#include "FightingGameProjectCharacter.generated.h"

UENUM(BlueprintType)
enum class EDirectionalInput : uint8 {

	VE_Default UMETA(DisplayName = "Neutral"),
	VE_MovingLeft UMETA(DisplayName = "Moving_Left"),
	VE_MovingRight UMETA(DisplayName = "Moving_Right")
};


UCLASS(config = Game)
class AFightingGameProjectCharacter : public ACharacter
{
	GENERATED_BODY()

		/** Side view camera */
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;


	
	virtual void Tick(float DeltaTime) override;

protected:
	// UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	// CharacterClass characterClass;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player References")
		AFightingGameProjectCharacter* otherPlayer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EDirectionalInput directionalInput;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float PlayerHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Super")
		float PlayerSuper;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		bool IsCrouching = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Model")
		bool isFlipped;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Model")
		FTransform transform;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Model")
		FVector scale;

	
public:
	AFightingGameProjectCharacter();
	void Crouch(float Value);
	void MoveRight(float Value);
	void MoveLeft(float Value);
	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
};
